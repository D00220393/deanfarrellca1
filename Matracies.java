package DeanFarrellCA1;

import java.util.Arrays;
import java.util.Scanner;


public class Matracies 
{
  
    Scanner kB = new Scanner(System.in);
  
    public int[] matrix1 = new int[12];
    public int[] matrix2 = new int[12];
    public int[] matrix3 = new int[12];
    
    
    public void createMatrices(int [] matrix1, int [] matrix2)
    {
////////CREATING THE 1ST MATRIX
        System.out.println("Please enter individually the values for the 4x3 Matrix");
        System.out.println("Example...");
        System.out.println("1  2  3  4\n5  6  7  8\n9  10 11 12");
        
        
        int postionCount = 1;
        
        for (int i = 0; i < matrix1.length; i++)
        {
            System.out.println("Please input value " + postionCount);
            matrix1[i] = kB.nextInt();
            postionCount++;
        }
        System.out.println("Your array is:" );
        printMatrix(matrix1);
        
        
////////CREATING THE 2ND MATRIX      
        postionCount = 1;
        
        for (int i = 0; i < matrix1.length; i++)
        {
            System.out.println("Please input value " + postionCount);
            matrix2[i] = kB.nextInt();
            postionCount++;
        }
        System.out.println("Your 2nd array is:" );
        printMatrix(matrix2);
    }
    
    
    public void addMatrices (int[] matrix1, int[] matrix2, int[] matrix3)
    {
        System.out.println("-------The Sum Is-------");
        printMatrix(matrix1);
        System.out.println("\t\t\t+");
        printMatrix(matrix2);
        
        
        for (int i = 0; i < matrix3.length; i++)
        {
            matrix3[i] = matrix1[i] + matrix2[i];
        }
        
        System.out.println("------------------------");
        printMatrix(matrix3);
        
        System.out.println("\n");
        System.out.println("The arrays added together make the new array: " + Arrays.toString(matrix3));
    }
    
    //PRINTS THE ARRAYS IN A 4X3 NEATLY
    private void printMatrix(int[] matrix)
    {
        int count4 = 1;
        for (int i = 0; i < matrix.length; i++)
        {
            System.out.printf("%5d", matrix[i]);
            if (count4 % 4 == 0)
            {
                System.out.println("");
            }
            count4++;
        }
    }
}
