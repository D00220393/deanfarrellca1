package DeanFarrellCA1;

import java.util.Arrays;
import java.util.Scanner;


public class MaxAndMin 
{
    Scanner kB = new Scanner(System.in);
    
    public int[] numArray = new int[5];
    
    public void createArray(int[] numArray)
    {        
        System.out.println("Please input the values you'd like for your array");
        for(int i = 0; i < numArray.length; i++)
        {
            numArray[i] = kB.nextInt();
        }
    }
    
    
    public void indexOfMinAndMax(int[] numArray)
    {
        int maxNum = numArray[1];
        int maxNumIndex = 0;
        int minNum = numArray[1];
        int minNumIndex = 0;
        
        for (int i = 0; i < numArray.length; i++)
        {
            if(numArray[i] > maxNum)
            {
                maxNum = numArray[i];
                maxNumIndex = i;
            }
            if(numArray[i] < minNum)
            {
                minNum = numArray[i];
                minNumIndex = i;
            }
        }
        
        int[] maxMinArray = new int[] {maxNum, maxNumIndex, minNum, minNumIndex};
        System.out.println("The max number is: " + maxMinArray[0] + " at index " + maxMinArray[1] + " in the array.");
        System.out.println("The min number is: " + maxMinArray[2] + " at index " + maxMinArray[3] + " in the array.");
        System.out.println(Arrays.toString(maxMinArray));
    }
}
