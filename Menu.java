package DeanFarrellCA1;

import java.util.Arrays;
import java.util.Scanner;


public class Menu 
{
    Scanner kB = new Scanner(System.in);
    
    private void printOptions()
    {
        System.out.println("Please select your option...\n");
        System.out.println("0. Exit");
        System.out.println("1. Matracies");
        System.out.println("2. Max & Min");
        
        System.out.println("9. Show Menu");
    }
    
    
    public void menu()
    {
        
        //MATRACIES
        Matracies matracies = new Matracies();
        int[] matrix1 = new int[12];
        int[] matrix2 = new int[12];
        int[] matrix3 = new int[12];
               
        //MAX AND MIN
        MaxAndMin maxandmin = new MaxAndMin();
        int[] numArray = new int[5];
        
        //The default i.e. prints the menu
        int questionSelect = 9;
        
        while (questionSelect != 0)
        {
            printOptions();
            questionSelect = kB.nextInt();
            
            switch (questionSelect)
            {
                case 1: 
                    matracies.createMatrices(matrix1, matrix2);
                    matracies.addMatrices(matrix1, matrix2, matrix3);
                    break;
                case 2:
                    maxandmin.createArray(numArray);
                    maxandmin.indexOfMinAndMax(numArray);
                    break;
                case 9:
                    printOptions();
                    
                    
            }
            
            if(questionSelect != 0)
            {
                System.out.println("\n\nWhat would you like to chose now?");
            }
            
            //CONFIRMS EXIT OF THE CODE
            if (questionSelect == 0)
            {
                int exit = 1;
                System.out.println("Are you sure?");
                System.out.println("1 = leave \t 0 = stay");
                exit = kB.nextInt();
                
                if(exit == 0)
                {
                    questionSelect = 9;
                }
            }   
        }
    } 
}
